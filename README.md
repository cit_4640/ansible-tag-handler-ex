# ansible-tag-handler-ex

## Example code from week 7 fall 2023 CIT 4640

I used the Terraform configuration from week 5 to create the infrastructure required  
to run the Ansible configuration included in this repository.

The 'ansible.cfg' file specifies an ssh private key `private_key_file = ~/.ssh/web-key`.  
You will either need to create this ssh key pair and use this in your terraform config,  
or change this line to use a different ssh key. 

## After cloning the repository.

- Create infrastructure using Terraform
- cd into the ansible directory in this repo
- look at the ansible playbook, 'webserver.yml' file.
- run the command `ansible-playbook webserver.yml --tags nginx`
- run the command `ansible-playbook webserver.yml --skip-tags nginx`

